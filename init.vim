" neovim-config
"                         _
"   _ __   ___  _____   _(_)_ __ ___
"  | '_ \ / _ \/ _ \ \ / / | '_ ` _ \
"  | | | |  __/ (_) \ V /| | | | | | |
"  |_| |_|\___|\___/ \_/ |_|_| |_| |_|
"


"  Options  =====================================

set expandtab           " convert tabs to spaces
set smarttab
set tabstop=2           " the number of spaces for a tab
set shiftwidth=2        " the number of spaces inserted for each indentation
set softtabstop=-1      " If negative, shiftwidth value is used

set cursorline          " highlight the current line
set number              " set numbered lines
set numberwidth=2       " set number column width to 2 {default 4}
"set relativenumber     " Show the line number relative to the line with the cursor

set nowrap              " display lines as one long line
set scrolloff=8         " number of screen lines to keep above and below the cursor
set sidescrolloff=8     " number of screen lines to keep on left and right side of cursor

set clipboard=unnamedplus   " allows neovim to access the system clipboard

set encoding=UTF-8      " the encoding written to a file
set hlsearch            " highlight all matches on previous search pattern
set ignorecase          " case insensitive searching...
set smartcase           " ...unless /C or capital in search
set mouse=a             " allow the mouse to be used in neovim

"set noshowmode

" Better editing experience
set cindent         " Enables automatic C program indenting.
set autoindent      " Copy indent from current line when starting a new line
set smartindent     " make indenting smarter again

"  Keymaps  =============================================

" Leader key
let mapleader = ' '

" Quit neovim
nnoremap <C-q> :q<CR>

" Quick save
nnoremap <C-s> :w<CR>

" leader-o/O inserts blank line below
nnoremap <leader>o o<ESC>

" Move to last buffer
"nnoremap '' :b#<CR>

" Copying the vscode behaviour of making tab splits
nnoremap <C-\> :vsplit<CR>

" Better window navigation
nnoremap <C-Left> <C-w>h
nnoremap <C-Down> <C-w>j
nnoremap <C-Up> <C-w>k
nnoremap <C-Right> <C-w>l
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Navigate between buffers
noremap <Leader><S-Right> :bn<CR>
noremap <Leader><S-Left> :bp<CR>
noremap <Leader><S-l> :bn<CR>
noremap <Leader><S-h> :bp<CR>

" Clear highlights
nnoremap <leader>h :nohl<CR>

" Move lines up and down
nnoremap <A-Down> :m .+1<CR>
nnoremap <A-Up> :m .-2<CR>
inoremap <A-Down> <Esc>:m .+1<CR>==gi
inoremap <A-Up> <Esc>:m .-2<CR>==gi
vnoremap <A-Down> :m '>+1<CR>gv=gv
vnoremap <A-Up> :m '<-2<CR>gv=gv
nnoremap <A-j> :m .+1<CR>
nnoremap <A-k> :m .-2<CR>
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv



